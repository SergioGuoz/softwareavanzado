from flask import Flask, jsonify, request

app = Flask(__name__)

entregas={
	"1":{"idUsuario":200,"estadoPedido":"En camino","producto":"Comida B","cantidad":1}
}

@app.route('/recibirPedido',methods=['POST'])
def recibirPedido():
	#Registrar la entrega
	nuevaEntrega={
		"idUsuario":request.json['idUsuario'],
		"idPedido":request.json['idPedido'],
		"direccion":request.json['direccion'],
		"estado":"recibido"
	}
	#Generar el ID de la Entrega
	idEntrega=len(entregas)+1

	#Guardar el pedidod en el diccionario de entregas
	entregas[str(idEntrega)]=nuevaEntrega

	#Retornar la informacion del pedido
	return jsonify({"mensaje":'El pedido fue recibido',"entrega":nuevaEntrega})

#Informar estado pedido al Cliente
@app.route('/estadoPedido/<int:idEntrega>',methods=['GET'])
def estadoPedido(idEntrega):
	if str(idEntrega) in entregas:
		respuesta=entregas[str(idEntrega)]
		return jsonify({"estadoRepartidor":respuesta['estadoPedido']})
	return {"estadoRepartidor":'No existe el pedido'}


#Marcar un pedido como marcado
@app.route('/marcarEntregaPedido',methods=['POST'])
def marcarEntregaPedido():
	
	#Mensaje de retorno
	return jsonify({"mensaje":"El pedido fue marcado como entregado"})
	


if __name__ == '__main__': 
	app.run(debug="True", port=4003)