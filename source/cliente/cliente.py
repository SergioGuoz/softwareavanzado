from flask import Flask, jsonify, request
import random
import requests
import json

app = Flask(__name__)


esbUrl='http://localhost:4000'
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}

#Para solicitar un pedido al restaurante
@app.route('/solicitarPedido',methods=['POST'])
def solicitarPedido():
	#Informacion del pedido que solicitara el cliente
	miPedido={
		"idUsuario":request.json['idUsuario'],
		"producto":request.json['producto'],
		"cantidad":request.json['cantidad'],
		"idRestaurante":request.json['idRestaurante']
	}
	
	resp = requests.post(esbUrl+'/solicitarPedidoESB', data=json.dumps(miPedido), headers=headers)
	respuesta=resp.json()

	#Retornar la informacion del pedido
	return jsonify({"mensaje":respuesta['mensaje']})


#Para verificar el estado de un Pedido en el restaurante
#Parametro idPedido realizado por el cliente
@app.route('/estadoPedidoRestaurante/<int:idPedido>',methods=['GET'])
def estadoPedido(idPedido):

	#Llamar al estado del pedido en el ESB
	resp = requests.get(esbUrl+'/estadoPedidoRestauranteESB/'+str(idPedido))
	respuesta=resp.json()

	return {"estadoPedido":respuesta['estadoPedido']}


#Para verificar el estado de un Pedido con el Repartidor
#Parametro idPedido realizado por el cliente
@app.route('/estadoPedidoRepartidor/<int:idPedido>',methods=['GET'])
def estadoPedidoRepartidor(idPedido):

	#Llamar al estado del pedido en el ESB
	resp = requests.get(esbUrl+'/estadoPedidoRepartidorESB/'+str(idPedido)).json()

	return {"estadoRepartidor":resp['estadoRepartidor']}
	


if __name__ == '__main__': 
	app.run(debug="True", port=4001)