import unittest
import restaurante 

class TestStringMethods(unittest.TestCase):


    def test_estado_pedido_invalido(self):
        self.assertEqual(restaurante.estadoPedidoValue(-1),'No existe el pedido')

    def test_estado_pedido_valido(self):
        self.assertIn(restaurante.estadoPedidoValue(1),['Preparando','Listo','En Camino'])

    def test_pedido_listo_respuesta_valida(self):
        self.assertIn(restaurante.isPedidoListo(1)['Listo'],['SI','NO']);

    def test_pedido_listo_no_valido(self):
        self.assertEqual(restaurante.isPedidoListo(-1)['Listo'],'No existe el pedido')

    def test_recibir_pedido_valido(self):
        self.assertIsNotNone(restaurante.recibirPedidoYGuardar(10,'Hamburguesa',1,1))



if __name__ == '__main__':
    unittest.main()