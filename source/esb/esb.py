from flask import Flask, jsonify, request
import requests
import random
import json

app = Flask(__name__)


url='http://localhost'

#Catalogo de puertos
puertos={"cliente":"4001","restaurante":"4002","repartidor":"4003"}

headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
#Para solicitar un pedido al restaurante por medio del ESB

@app.route('/solicitarPedidoESB',methods=['POST'])
def solicitarPedidoESB():
	#Informacion del pedido que solicitara el cliente
	print(request.json['idUsuario'])
	miPedido={
		"idUsuario":request.json['idUsuario'],
		"producto":request.json['producto'],
		"cantidad":request.json['cantidad'],
		"idRestaurante":request.json['idRestaurante']
	}
	
	resp = requests.post(url+':'+puertos['restaurante']+'/recibirPedido', json=miPedido, headers=headers)
	print(resp)
	respuesta=resp.json()
	#print(respuesta['mensaje']+' idPedido '+respuesta['idPedido'])
	#Retornar la informacion del pedido
	return jsonify({"mensaje":respuesta['mensaje']+' idPedido:'+str(respuesta['idPedido'])})


#Para verificar el estado de un Pedido en el restaurante
#Parametro idPedido realizado por el cliente
@app.route('/estadoPedidoRestauranteESB/<int:idPedido>',methods=['GET'])
def estadoPedidoRestauranteESB(idPedido):

	#Peticion al restaurante
	resp = requests.get(url+':'+puertos['restaurante']+'/estadoPedido/'+str(idPedido))
	respuesta=resp.json()
	return {"estadoPedido":respuesta['estadoPedido']}


#Para verificar el estado de un Pedido con el Repartidor
#Parametro idPedido realizado por el cliente
@app.route('/estadoPedidoRepartidorESB/<int:idPedido>', methods=['GET'])
def estadoPedidoRepartidorESB(idPedido):

	#Peticion al repartidor
	resp = requests.get(url+':'+puertos['repartidor']+'/estadoPedido/'+str(idPedido))
	respuesta=resp.json()
	return {"estadoRepartidor":respuesta['estadoRepartidor']}
	
#Marcar un pedido como entregado
@app.route('/marcarEntregaPedidoESB',methods=['POST'])
def marcarEntregaPedidoESB():
	
	#Pedido del cliente
	idPedido=str(request.json['idPedido'])
	
	#Peticion al repartidor
	resp = requests.post(url+':'+puertos['repartidor']+'/marcarEntregaPedido', data=json.dumps({"idPedido":idPedido}), headers=headers)
	respuesta=resp.json()
	#Mensaje de retorno
	return jsonify({"mensaje":respuesta['mensaje']})

if __name__ == '__main__': 
	app.run(debug="True", port=4000)