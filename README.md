# SoftwareAvanzado
### Practica 6

# SonarQube

Se ejecuta SonarQube para medir la calidad del código.

Para ejecutar sonarqube, corremos el siguiente comando en la carpeta en donde se encuentra nuestro proyecto o 
el código que deseamos analizar.

* 1. sonar.sources=. indica la dirección del código
* 2. sonar.login=5c68c76c7729888c943ede1a7b150af36503eead un token generado en sonarqube.
* 3. sonar.host.url=http://localhost:9000 url donde se visualizará

```python
sonar-scanner.bat -D"sonar.projectKey=practica6" -D"sonar.sources=." -D"sonar.host.url=http://localhost:9000" -D"sonar.login=5c68c76c7729888c943ede1a7b150af36503eead" -D"sonar.exclusions=**api.json" -D"sonar.python.unittest=test.py" -D"sonar.exclusions=test.py" -D"sonar.tests=test.py" 
```
## Pruebas Unitarias
### Unittest
Se utiliza esta librería para ejecutar pruebas automatizadas cubriendo las funcionalidades del archivo restaurante.py


Importamos librerías, unittest para las pruebas y restaurante es la clase a la que realizaremos las pruebas.
```python
import unittest
import restaurante 
```

### Pruebas
Le asignamos un nombre que especifique lo que la prueba esta haciendo y luego llamamos al metodo assert para comprobar los resultados 
y compararlo con los valores que deberían retornar para pasar la prueba.

```python

class TestRestaurante(unittest.TestCase):


    def test_estado_pedido_invalido(self):
        self.assertEqual(restaurante.estadoPedidoValue(-1),'No existe el pedido')

    def test_estado_pedido_valido(self):
        self.assertIn(restaurante.estadoPedidoValue(1),['Preparando','Listo','En Camino'])

    def test_pedido_listo_respuesta_valida(self):
        self.assertIn(restaurante.isPedidoListo(1)['Listo'],['SI','NO']);

    def test_pedido_listo_no_valido(self):
        self.assertEqual(restaurante.isPedidoListo(-1)['Listo'],'No existe el pedido')

    def test_recibir_pedido_valido(self):
        self.assertIsNotNone(restaurante.recibirPedidoYGuardar(10,'Hamburguesa',1,1))
```



## PRUEBAS DE FUNCIONALIDAD 🛠️
[YouTube]

## Autor ✒️

* **Sergio Geovany Guoz Tubac** 🤓